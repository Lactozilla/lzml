# LZML (LactoZilla's Math Library)

This is a replacement for cglm that I wrote. Unlike cglm, lzml offers no optimizations. It also doesn't define any types; every function uses C's `float` type, or arrays of `float`. Its only "dependency" is the `math.h` header.

Most of lzml's functions are similar in usage to cglm. Notable differences are a change in the position of a parameter (for example, `glm_perspective` vs `lzml_matrix4_perspective`), or excluding a "destination" parameter (like with `glm_mat4_mul` vs `lzml_matrix4_multiply`, although `lzml_matrix4_multiply_into` still exists for that kind of usage).

Feel free to modify this library for your purposes (e.g., a version that uses fixed-point math, or includes SIMD optimizations).

## Usage

Simply include `lzml.h`.
